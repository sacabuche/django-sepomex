from django.conf.urls.defaults import patterns, include

from tastypie.api import Api
from sepomex.api.resources import (SettlementResource, PostalCodeResource,
                                   StateResource, MunicipalityResource,
                                   ZoneResource, SettlementTypeResource)

v1_api = Api(api_name='v1')
v1_api.register(SettlementResource())
v1_api.register(PostalCodeResource())
v1_api.register(StateResource())
v1_api.register(MunicipalityResource())
v1_api.register(ZoneResource())
v1_api.register(SettlementTypeResource())

urlpatterns = patterns('',
        (r'^sepomex/', include(v1_api.urls)),
)
