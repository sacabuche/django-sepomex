from django.core.management.base import BaseCommand
from optparse import make_option
from sepomex.utils import load_csv_file


class Command(BaseCommand):

    args = 'file_name.csv'
    help = 'load the csv file from sepomex with "|"'

    option_list = BaseCommand.option_list + (
        make_option('--start_on',
                    dest='start_on',
                    type="int",
                    default=0,
                    help='start to read from line number: x'),
    )

    def handle(self, file_path, **options):
        load_csv_file(file_path, **options)
        self.stdout.write("All loaded")
