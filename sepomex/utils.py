import csv

from models import State, Municipality, PostalCode, SettlementType
from models import Zone, Settlement


class AttrDict(object):

    def __init__(self, dictionary, mapper=dict()):
        self._dict = dictionary
        self._mapper = mapper

    def __getattr__(self, name):
        real_name = self._mapper.get(name, name)
        try:
            return self._dict[real_name]
        except KeyError:
            raise AttributeError(name)


class SepomexCsv(object):

    FIELDS = {
        'CP': 'd_codigo',
        'state': 'd_estado',
        'municipality': 'D_mnpio',
        'settlement': 'd_asenta',
        'zone': 'd_zona',
        'settlement_type': 'd_tipo_asenta',
    }

    def __init__(self, file_path, start_on):
        self.file_path = file_path
        self.settlements = []
        self.zones = set()
        self.types = set()
        self.states = dict()
        self.cp = set()
        self.objs = dict()
        self.start_on = start_on
        self._cache = dict()

    def load(self):
        with open(self.file_path, 'r') as csv_file:
            self.reader = csv.DictReader(csv_file, delimiter='|')
            [self.reader.next() for x in range(self.start_on)]
            for row in self.reader:
                self.add_row(row)

    def add_row(self, row):
        row = AttrDict(row, self.FIELDS)
        self.cp.add(row.CP)
        print row.CP
        self.states.setdefault(row.state, set()).add(row.municipality)
        self.zones.add(row.zone)
        self.types.add(row.settlement_type)
        self.objs.setdefault(row.state, {}).\
                setdefault(row.municipality, {}).\
                setdefault(row.CP, {}).\
                setdefault(row.zone, {}).\
                setdefault(row.settlement_type, set()).\
                add(row.settlement)

    def save(self):
        all_settlements = []
        self.save_cps()
        self.save_states()
        self.save_zones()
        self.save_municipalities()
        self.save_types()
        print "Building Settlements"
        for state, municipalities in self.objs.items():
            print "Settlements of:", state
            state = State.objects.get(name=state)
            for mun_name, CPs in municipalities.items():
                mun_obj = Municipality.objects.get(name=mun_name, state=state)
                for CP, zones in CPs.items():
                    CP = PostalCode.objects.get(number=CP)
                    for zone, types in zones.items():
                        zone = Zone.objects.get(name=zone)
                        for type_, settlements in types.items():
                            type_ = SettlementType.objects.get(name=type_)
                            settlements = [Settlement(municipality=mun_obj,
                                                      postal_code=CP,
                                                      type=type_,
                                                      zone=zone,
                                                      name=st_name)
                                           for st_name in settlements]
                            all_settlements.extend(settlements)
        self.bulk_create(Settlement, all_settlements, 100)

    def save_cps(self):
        cps = [PostalCode(number=number) for number in self.cp]
        self.bulk_create(PostalCode, cps)

    def save_zones(self):
        zones = [Zone(name=name) for name in self.zones]
        self.bulk_create(Zone, zones)

    def save_types(self):
        types = [SettlementType(name=name) for name in self.types]
        self.bulk_create(SettlementType, types)

    def save_states(self):
        states = [State(name=name) for name in self.states.keys()]
        self.bulk_create(State, states)

    def save_municipalities(self):
        all_mun = []
        for state, municipalities in self.states.items():
            state = State.objects.get(name=state)
            m = [Municipality(state=state, name=name)
                 for name in municipalities]
            all_mun.extend(m)
        self.bulk_create(Municipality, all_mun)

    def bulk_create(self, cls, objs, batch_size=200):
        size = len(objs)
        for i in xrange(0, len(objs), batch_size):
            slice_ = objs[i:i + batch_size]
            cls.objects.bulk_create(slice_)
            print "created : ", cls.__name__, i + len(slice_), "of", size


def load_csv_file(file_path, **options):
    start_on = options.get('start_on', 0)
    loader = SepomexCsv(file_path, start_on=start_on)
    loader.load()
    loader.save()
